# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import humanize
import datetime


def naturaltime(_, date):
    return humanize.naturaltime(date, when=datetime.datetime.utcnow())
